##
## Part of configuration files for Zsh 4
## by Hugues Hiegel <hugues@hiegel.fr>
##
## NO WARRANTY PROVIDED, USE AT YOUR OWN RISKS
##
## You are encouraged to use, modify, and redistribute
## these files with or without this notice.
##

# NVM stuff
NVM_ENV=~/.nvm/nvm.sh
if [ -r ${NVM_ENV} ]
then
	source ${NVM_ENV}
	_nvm_env() {
		which nvm 2>&- >&- && echo "nvm:$(nvm current)"
	}

	PS1_TASKBAR+=(_nvm_env)
fi

